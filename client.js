const net = require('net');

const readLine = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
})

const awaitUsername = new Promise(resolve => {
    readLine.question('Enter username to join: ', username => {
        resolve(username);
    });
});

awaitUsername.then(username => {

    const socket = net.connect({
        port: 5000
    })

    socket.on('connect', () => {
        socket.write(`${username} joined the chat.`)
    })


    readLine.on('line', data => {
        if (data === 'quit') {
            socket.write(`${username} has left the chat.`)
            socket.setTimeout(1000);
        } else {
            socket.write(`${username} :  ${data}`)
        }
    });

    socket.on('data', data => {

        console.log(`\x1b[1m\x1b[34m ${data}`);
    })
    socket.on('timeout', () => {
        socket.write('quit');
        socket.end();
    })
    socket.on('end', () => {
        process.exit();
    })

    socket.on('error', () => {
        console.log('The server shut dowwn unexpectedly')
    })
})

