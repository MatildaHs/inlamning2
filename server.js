const net = require('net');

let sockets = [];

const server = net.createServer(socket => {

    sockets.push(socket);
    console.log('\x1b[1m\x1b[32m Client connected')


    socket.on('data', data => {
        broadcast(data, socket)
    })


    socket.on('error', err => {
        console.log('x1b[1m\x1b[31m Client disconnected')
    })
    socket.on('close', () => {
        console.log('\x1b[1m\x1b[31m A client left the room')
    })

});


function broadcast(message, socketSent) {
    const index = sockets.indexOf(socketSent)
    if (message === 'quit') {
        sockets.splice(index, 1)
    }
    else {
        sockets.forEach(socket => {
            if (socket !== socketSent) socket.write(message);
        });
    }
}

server.listen(5000)
